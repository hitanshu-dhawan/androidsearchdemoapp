package com.example.hitanshu.search.network;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by hitanshu on 14/7/17.
 */

public class CountriesAsyncTask extends AsyncTask<String, Void, ArrayList<String> > {

    OnCountriesDownloadListner mListner;

    public CountriesAsyncTask(OnCountriesDownloadListner listner) {
        this.mListner = listner;
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {

        try {
            URL url = new URL(params[0]);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.connect();

            if(httpURLConnection.getResponseCode() != 200) return null;

            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream);
            String jsonString = "";
            while(scanner.hasNext()) {
                jsonString += scanner.nextLine();
            }

            // Parse JSON
            ArrayList<String> countries = new ArrayList<>();
            JSONArray countriesJsonArray = new JSONArray(jsonString);
            for(int i=0; i < countriesJsonArray.length(); i++) {
                JSONObject country = (JSONObject) countriesJsonArray.get(i);
                countries.add(country.getString("name"));
            }
            return countries;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        mListner.onCountriesDownloadComplete(strings);
    }
}
