package com.example.hitanshu.search.network;

import java.util.ArrayList;

/**
 * Created by hitanshu on 14/7/17.
 */

public interface OnCountriesDownloadListner {
    void onCountriesDownloadComplete(ArrayList<String> strings);
}
