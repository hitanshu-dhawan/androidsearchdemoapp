package com.example.hitanshu.search;

import android.app.SearchManager;
import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.hitanshu.search.network.CountriesAsyncTask;
import com.example.hitanshu.search.network.OnCountriesDownloadListner;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    String urlString = "https://restcountries.eu/rest/v2/name/";

    CountriesAsyncTask countriesAsyncTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu,menu);

        listView = (ListView) findViewById(R.id.listView);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_list_item_1,arrayList);
        listView.setAdapter(arrayAdapter);

        MenuItem searchMenuItem = menu.findItem(R.id.search_item);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                if(countriesAsyncTask != null) {
                    countriesAsyncTask.cancel(true);
                    countriesAsyncTask = null;
                }

                if(newText.trim().toString().isEmpty()) {
                    arrayList.clear();
                    arrayAdapter.notifyDataSetChanged();
                    return false;
                }

                countriesAsyncTask = new CountriesAsyncTask(new OnCountriesDownloadListner() {
                    @Override
                    public void onCountriesDownloadComplete(ArrayList<String> strings) {
                        arrayList.clear();
                        if(strings != null)
                            arrayList.addAll(strings);
                        arrayAdapter.notifyDataSetChanged();
                    }
                });
                countriesAsyncTask.execute(urlString+newText);

                return false;
            }
        });

        return true;
    }
}
